from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import groups, groups_comments, groups_members
from routers import plants
from routers import accounts, gardens, blogs, blogsposts, blogsposts_comments
from authenticator import authenticator
import os

app = FastAPI(debug=True)

# Middleware for CORS
origins = [
    os.environ.get("CORS_HOST", None),
    "http://localhost:3000",
    "https://04-floridaman.gitlab.io",
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Include Routers
app.include_router(authenticator.router)
app.include_router(groups_comments.router)
app.include_router(groups.router)
app.include_router(groups_members.router)
app.include_router(plants.router)
app.include_router(accounts.router)
app.include_router(gardens.router)
app.include_router(blogs.router)
app.include_router(blogsposts.router)
app.include_router(blogsposts_comments.router)


@app.get("/")
def read_root():
    return {"Hello": "World"}
