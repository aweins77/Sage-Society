from fastapi import APIRouter, HTTPException, Depends, Body
from typing import List
from queries.groups_comments import (
    GroupCommentRepository,
    GroupCommentIn,
    GroupCommentOut,
    Error,
)
from queries.accounts import AccountOut
from authenticator import authenticator

router = APIRouter()
comment_repository = GroupCommentRepository()


@router.post(
    "/group/{groups_id}/comment",
    response_model=GroupCommentOut,
    tags=["GroupComments"],
    operation_id="create_comment_for_group",
    status_code=201,
)
def create_comment_for_group(
    groups_id: int,
    comment: GroupCommentIn = Body(...),
    _: AccountOut = Depends(authenticator.get_current_account_data),
):
    # Authentication is enforced. If not get_current_account_datad, it won't proceed to this function.

    result = comment_repository.create_comment(comment)

    if isinstance(result, Error):
        raise HTTPException(status_code=400, detail=result.message)

    return result


@router.get(
    "/comment/{comments_id}",
    response_model=GroupCommentOut,
    tags=["GroupComments"],
    operation_id="get_comment_by_id",
)
def get_comment_detail(
    comments_id: int,
    _: AccountOut = Depends(authenticator.get_current_account_data),
):
    # Authentication is enforced. If not get_current_account_datad, it won't proceed to this function.

    result = comment_repository.get_comment(comments_id)

    if isinstance(result, Error):
        raise HTTPException(status_code=404, detail=result.message)

    return result


@router.get(
    "/group/{groups_id}/comments",
    response_model=List[GroupCommentOut],
    tags=["GroupComments"],
    operation_id="get_comments_for_group",
)
def get_comments_for_group(
    groups_id: int,
    _: AccountOut = Depends(authenticator.get_current_account_data),
):
    # Authentication is enforced. If not get_current_account_datad, it won't proceed to this function.

    results = comment_repository.get_comments_for_group(groups_id)

    if isinstance(results, Error):
        raise HTTPException(status_code=404, detail=results.message)

    return results
