from fastapi import APIRouter, Depends, HTTPException, status
from typing import Union
from queries.blogs import BlogIn, BlogOut, BlogRepository, Error, BlogListOut

from authenticator import authenticator

router = APIRouter()


@router.post("/blogs", response_model=BlogOut, tags=["Blogs"])
def create_blog(
    blogs: BlogIn,
    current_account=Depends(authenticator.get_current_account_data),
    repo: BlogRepository = Depends(),
):
    return repo.create(blogs)


@router.get("/blogs", response_model=Union[BlogListOut, Error], tags=["Blogs"])
def get_blogs(
    repo: BlogRepository = Depends(),
    current_account=Depends(authenticator.get_current_account_data),
):
    return repo.get_blogs()


@router.get("/blogs/account", response_model=Union[Error, BlogOut], tags=["Blogs"])
def get_blog(
    current_account: dict = Depends(authenticator.get_current_account_data),
    repo: BlogRepository = Depends(),
):
    accounts_id = current_account["id"]
    return repo.get_blog_by_account(accounts_id)


@router.get(
    "/blogs/{blogs_id}", response_model=BlogOut, tags=["Blogs"]
)
def get_blog(
    blogs_id: int,
    current_account=Depends(authenticator.get_current_account_data),
    repo: BlogRepository = Depends(),
) -> BlogOut:
    blog = repo.get_blog_by_id(blogs_id)
    if blog is None:
        raise HTTPException(
            status_code=404,
            detail="Blog not found",
        )
    return blog


@router.put(
    "/blogs/{blogs_id}", response_model=BlogOut, tags=["Blogs"]
)
def update_blog(
    blogs_id: int,
    updated_blog: BlogIn,
    current_account=Depends(authenticator.get_current_account_data),
    repo: BlogRepository = Depends(),
) -> BlogOut:
    accounts_id = current_account["id"]
    return repo.update(blogs_id, accounts_id, updated_blog)


@router.delete(
    "/blogs/{blogs_id}", response_model=bool, tags=["Blogs"]
)
def delete_blog(
    blogs_id: int,
    repo: BlogRepository = Depends(),
    current_account=Depends(authenticator.get_current_account_data),
) -> bool:
    accounts_id = current_account["id"]
    response = repo.delete(blogs_id, accounts_id)
    if response is None:
        return False
    else:
        return True
