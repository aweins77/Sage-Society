steps = [
    # For the tables that were created in 002.py but had incorrect DROP commands, just provide the corrected DROP command.
    [
        """
    """,
        """
    DROP TABLE gardens;
    """,
    ],
    [
        """
    """,
        """
    DROP TABLE blogs;
    """,
    ],
    [
        """
    """,
        """
    DROP TABLE blogs_post;
    """,
    ],
    [
        """
    """,
        """
    DROP TABLE blogs_post_comment;
    """,
    ],
]
