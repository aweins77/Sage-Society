from queries.pool import pool
from typing import Union, List, Optional
from pydantic import BaseModel
from fastapi import HTTPException, status

class Error(BaseModel):
    message: str


class BlogIn(BaseModel):
    accounts_id: int
    title: str


class BlogOut(BaseModel):
    blogs_id: int
    accounts_id: int
    title: str


class BlogListOut(BaseModel):
    blogs: List[BlogOut]


class BlogDeleteResponse(BaseModel):
    blogs_id: int
    message: str


class BlogRepository(BaseModel):
    def blog_in_to_out(self, blogs_id: int, blogs: BlogIn) -> BlogOut:
        old_data = blogs.dict()
        return BlogOut(blogs_id=blogs_id, **old_data)

    def record_to_blog_out(self, record) -> BlogOut:
        return BlogOut(
            blogs_id=record[0],
            accounts_id=record[1],
            title=record[2],
        )

    def create(self, blog: BlogIn) -> Optional[BlogOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO blogs
                            (accounts_id, title)
                        VALUES
                            (%s, %s)
                        RETURNING blogs_id;
                        """,
                        [blog.accounts_id, blog.title],
                    )
                    id = result.fetchone()[0]
                    return self.blog_in_to_out(id, blog)
        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="Internal Server Error - Could not create new blog",
            )

    def get_blogs(self) -> Optional[BlogListOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT blogs_id,
                               accounts_id,
                               title
                        FROM blogs
                        ORDER BY accounts_id
                        """
                    )
                    return BlogListOut(
                        blogs=[
                            self.record_to_blog_out(record)
                            for record in result
                        ]
                    )
        except Exception:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="Internal Server Error - Could not get all blogs",
            )

    def get_blog_by_id(self, blogs_id: int) -> Optional[BlogOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT blogs_id,
                               accounts_id,
                               title
                        FROM blogs
                        WHERE blogs_id = %s
                        """,
                        [blogs_id],
                    )
                    record = db.fetchone()
                    if record:
                       return self.record_to_blog_out(record)
        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="Internal Server Error",
            )

    def get_blog_by_account(self, accounts_id: int) -> Union[BlogOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT blogs_id, accounts_id, title
                        FROM blogs
                        WHERE accounts_id = %s
                        """,
                        [accounts_id],
                    )
                    record = db.fetchone()
                    if record:
                        return self.record_to_blog_out(record)
                    else:
                        return Error(message="Blog not found for account" + accounts_id.__str__())
        except Exception as e:
            print(e)
            return Error(e)


    def update(
        self, blogs_id: int, accounts_id: int, updated_blog: BlogIn
    ) -> Optional[BlogOut]:
        try:
            blog = self.get_blog_by_id(blogs_id)

            if not blog:
                raise HTTPException(
                    status_code=404,
                    detail="Not Found",
                )

            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT blogs_id, accounts_id
                        FROM blogs
                        WHERE blogs_id = %s
                        """,
                        [blogs_id],
                    )

                    row = db.fetchone()
                    if row:
                        record = {
                            column.name: value
                            for value, column in zip(row, db.description)
                        }

                        if record["accounts_id"] != accounts_id:
                            raise HTTPException(
                                status_code=status.HTTP_403_FORBIDDEN,
                                detail="Not Authorized",
                            )
                        else:
                            db.execute(
                                """
                                UPDATE blogs
                                SET
                                    title = %s
                                WHERE blogs_id = %s
                                """,
                                [updated_blog.title, blogs_id],
                            )
                            return BlogOut(
                                blogs_id=blogs_id,
                                accounts_id=record["accounts_id"],
                                title=updated_blog.title,
                            )
        except Exception as e:
            print(e)
            raise


    # Needs to be updated to delete blog posts first if any
    def delete(self, blogs_id: int, accounts_id: int) -> Optional[BlogDeleteResponse]:
        try:
            blog = self.get_blog_by_id(blogs_id)

            if not blog:
                raise HTTPException(
                    status_code=404,
                    detail="Not Found",
                )

            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT blogs_id,
                               accounts_id,
                               title
                        FROM blogs
                        WHERE blogs_id = %s
                        """,
                        [blogs_id],
                    )

                    row = db.fetchone()
                    if row:
                        record = {
                            column.name: value
                            for value, column in zip(row, db.description)
                        }

                        if record["accounts_id"] != accounts_id:
                            raise HTTPException(
                                status_code=status.HTTP_403_FORBIDDEN,
                                detail="Not authorized to delete this blog.",
                            )
                        else:
                            db.execute(
                                """
                                DELETE from blogs
                                WHERE blogs_id = %s
                                """,
                                [blogs_id],
                            )
                            return BlogDeleteResponse(
                                blogs_id=blogs_id,
                                message="Blog entry deleted successfully.",
                            )
        except Exception as e:
            print(e)
            raise
