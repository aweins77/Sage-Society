from typing import List, Optional
from pydantic import BaseModel
from fastapi import HTTPException, status
from queries.pool import pool


class GardenOut(BaseModel):
    gardens_id: int
    gardens_name: str
    description: str
    accounts_id: int
    zip_code: int


class GardenListOut(BaseModel):
    gardens: List[GardenOut]


class GardenIn(BaseModel):
    gardens_name: str
    description: str
    accounts_id: int
    zip_code: int


class GardenUpdate(BaseModel):
    gardens_name: Optional[str] = None
    description: Optional[str] = None
    zip_code: Optional[int] = None


class GardenQueries:
    def get_all_gardens(self, accounts_id) -> List[GardenOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT gardens_id, gardens_name, description, accounts_id, zip_code
                    FROM gardens
                    WHERE accounts_id = %s
                    ORDER BY gardens_id
                    """,
                    [accounts_id],
                )

                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(GardenOut(**record))

                return results

    def get_gardens_by_account(self, accounts_id: int) -> List[GardenOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT gardens_id, gardens_name, description, accounts_id, zip_code
                    FROM gardens
                    WHERE accounts_id = %s
                    ORDER BY gardens_id
                    """,
                    [accounts_id],
                )

                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                    results.append(GardenOut(**record))

                return results

    def get_garden(self, gardens_id, accounts_id) -> Optional[GardenOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT gardens_id, gardens_name, description, accounts_id, zip_code
                    FROM gardens
                    WHERE gardens_id = %s
                    """,
                    [gardens_id],
                )

                row = cur.fetchone()
                if row:
                    record = {
                        column.name: value
                        for value, column in zip(row, cur.description)
                    }

                    if record["accounts_id"] != accounts_id:
                        raise HTTPException(
                            status_code=status.HTTP_403_FORBIDDEN,
                            detail="Not authorized to access this garden.",
                        )

                    return GardenOut(**record)

    def create_garden(self, data) -> Optional[GardenOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.gardens_name,
                    data.description,
                    data.accounts_id,
                    data.zip_code,
                ]
                cur.execute(
                    """
                    INSERT INTO gardens (gardens_name, description, accounts_id, zip_code)
                    VALUES (%s, %s, %s, %s)
                    RETURNING gardens_id, gardens_name, description, accounts_id, zip_code
                    """,
                    params,
                )

                row = cur.fetchone()
                if row:
                    record = {
                        column.name: value
                        for value, column in zip(row, cur.description)
                    }
                    return GardenOut(**record)

    def delete_garden(self, gardens_id, accounts_id) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM gardens
                    WHERE gardens_id = %s AND accounts_id = %s
                    """,
                    [gardens_id, accounts_id],
                )
        return True

    def update_garden(
        self, gardens_id, accounts_id, data: GardenUpdate
    ) -> Optional[GardenOut]:
        try:
            garden = self.get_garden(gardens_id, accounts_id)

            if not garden:
                return None

            update_data = {
                key: value
                for key, value in data.dict().items()
                if value is not None
            }

            set_clause = ", ".join(
                [f"{key} = %s" for key in update_data.keys()]
            )

            if update_data:
                with pool.connection() as conn:
                    with conn.cursor() as cur:
                        params = list(update_data.values()) + [gardens_id]
                        cur.execute(
                            f"""
                            UPDATE gardens
                            SET {set_clause}
                            WHERE gardens_id = %s
                            RETURNING gardens_id, gardens_name, description, accounts_id, zip_code
                            """,
                            params,
                        )

                        row = cur.fetchone()
                        if row:
                            record = {
                                column.name: value
                                for value, column in zip(row, cur.description)
                            }
                            return GardenOut(**record)
            else:
                return None
        except Exception:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="Internal Server Error",
            )
