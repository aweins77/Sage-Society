from queries.pool import pool
from typing import Union, List
from pydantic import BaseModel


class Error(BaseModel):
    message: str


class BlogPostDeleteResponse(BaseModel):
    blogs_post_id: int
    message: str


class BlogPostIn(BaseModel):
    blogs_id: int
    title: str
    picture_url: str
    text: str


class BlogPostOut(BaseModel):
    blogs_post_id: int
    blogs_id: int
    title: str
    picture_url: str
    text: str


class BlogPostList(BaseModel):
    blogposts: List[BlogPostOut]


class BlogPostRepository(BaseModel):
    def blogpost_in_to_out(self, blogs_post_id: int, blogpost: BlogPostIn):
        old_data = blogpost.dict()
        return BlogPostOut(blogs_post_id=blogs_post_id, **old_data)

    def record_to_blogpost_out(self, record):
        result = BlogPostOut(
            blogs_post_id=record[0],
            blogs_id=record[1],
            title=record[2],
            picture_url=record[3],
            text=record[4],
        )
        return result

    def create(self, blogpost: BlogPostIn, blogs_id: int) -> BlogPostOut:
        if blogpost.blogs_id != blogs_id:
            blogpost.blogs_id = blogs_id

        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                INSERT INTO blogs_post
                    (
                    blogs_id,
                    title,
                    picture_url,
                    text
                    )
                VALUES
                    (%s, %s, %s, %s)
                RETURNING blogs_post_id;
                """,
                        [
                            blogs_id,
                            blogpost.title,
                            blogpost.picture_url,
                            blogpost.text,
                        ],
                    )
                    id = result.fetchone()[0]
                    return self.blogpost_in_to_out(id, blogpost)
        except Exception as e:
            print(e)
            return {"message": "Could not create new BlogPost"}

    def get_all_blogposts(self, blogs_id: int) -> Union[BlogPostList, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            blogs_post_id,
                            blogs_id,
                            title,
                            picture_url,
                            text
                        FROM blogs_post
                        WHERE blogs_id = %s
                        ORDER BY blogs_post_id
                        """,
                        [blogs_id],
                    )
                    result = db.fetchall()
                    # print("this is the result", result)
                    return [
                        self.record_to_blogpost_out(record)
                        for record in result
                    ]

        except Exception as e:
            print(e)
        return {"message": "Could not get all blogposts"}

    def get_one_blogpost(self, blogspost_id: int) -> BlogPostOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                  SELECT blogs_post_id,
                         blogs_id,
                         title,
                         picture_url,
                         text
                  FROM blogs_post
                  WHERE blogs_post_id = %s
                  """,
                        [blogspost_id],
                    )

                    record = db.fetchone()
                    print(record)
                    return self.record_to_blogpost_out(record)

        except Exception as e:
            print(e)
            return {"message": "Could not get blogpost"}

    def delete(
        self, blogs_post_id: int
    ) -> Union[BlogPostDeleteResponse, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE from blogs_post
                        WHERE blogs_post_id = %s

                        """,
                        [blogs_post_id],
                    )
                    return BlogPostDeleteResponse(
                        blogs_post_id=blogs_post_id,
                        message="Blog Post was deleted successfully.",
                    )
        except Exception as e:
            print(e)
            return Error(message="Could not delete the Blog post")

    def update(
        self, blogs_post_id: int, updated_blog_post: BlogPostOut
    ) -> Union[BlogPostOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                    UPDATE blogs_post
                    SET blogs_id = %s,
                        title = %s,
                        picture_url= %s,
                        text = %s
                    WHERE blogs_post_id = %s
                    """,
                        [
                            updated_blog_post.blogs_id,
                            updated_blog_post.title,
                            updated_blog_post.picture_url,
                            updated_blog_post.text,
                            updated_blog_post.blogs_post_id,
                        ],
                    )

                    updated_blog_post.blogs_post_id = blogs_post_id
                    return updated_blog_post

        except Exception as e:
            print(e)
            return {"message": "Could not update blog"}
