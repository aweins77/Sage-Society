/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        "1acd81": "#1acd81",
        "#126945": "#126945",
      },
    },
  },
  plugins: [],
};
