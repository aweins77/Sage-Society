import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import useToken from "@galvanize-inc/jwtdown-for-react";

const fetchGardenData = async (gardens_id, setFormData, token) => {
  try {
    const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/gardens/${gardens_id}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
    const data = await response.json();
    if (response.ok) {
      setFormData(data);
    } else {
      throw new Error(data.message);
    }
  } catch (error) {
    console.error("An error occurred while fetching garden data:", error);
  }
};

const EditGardenForm = () => {
  const [formData, setFormData] = useState({
    gardens_name: '',
    description: '',
    zip_code: '',
    accounts_id: '',
  });
  const { token } = useToken();
  const navigate = useNavigate();
  const { gardens_id } = useParams();

  useEffect(() => {
    if (token) {
    fetchGardenData(gardens_id, setFormData, token);
    }
  }, [gardens_id, token]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/gardens/${gardens_id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`
        },
        body: JSON.stringify(formData),
      });

      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(`Server responded with ${response.status}: ${errorData.detail}`);
      }

      navigate('/gardens'); // take user back to garden list
    } catch (error) {
      console.error("An error occurred while updating the garden:", error);
    }
  };

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-100">
      <div className="bg-white p-8 rounded-lg shadow-md w-full max-w-2xl">
        <form onSubmit={handleSubmit} className="space-y-4">
          <h1 className="text-3xl mb-6 text-center font-bold">Edit Garden</h1>
          <div className="flex flex-col space-y-2">
            <label className="block text-sm font-bold mb-2">
              Garden Name:
            </label>
            <input
              type="text"
              name="gardens_name"
              value={formData.gardens_name}
              onChange={handleChange}
              required className="shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div>
            <label className="block text-sm font-bold mb-2">
              Description:
            </label>
            <input
              type="text"
              name="description"
              value={formData.description}
              onChange={handleChange}
              required className="shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div>
            <label className="block text-sm font-bold mb-2">
              Zip Code:
            </label>
            <input
              type="text"
              name="zip_code"
              value={formData.zip_code}
              onChange={handleChange}
              required className="shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <button
            type="submit"
            className="bg-blue-500 text-white p-2 rounded hover:bg-blue-600">Update Garden</button>
        </form>
      </div>
    </div>
  );
};

export default EditGardenForm;
