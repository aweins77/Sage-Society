import React from "react";
import { NavLink } from "react-router-dom"; // <-- Importing NavLink
import background from "./pexels-karolina-grabowska-4750270.jpg";

function MainPage() {
  return (
    <>
      <div
        style={{ backgroundImage: `url(${background})` }}
        className="p-8 flex flex-col items-center justify-center min-h-screen bg-cover bg-center bg-no-repeat"
      >
        <h1 className="text-6xl lg:text-7xl capitalize mb-12 text-white font-semibold">
          Welcome in
        </h1>
        <div className="flex flex-col items-center lg:w-1/2 mx-auto">
          {" "}
          {/* Added flex-col and items-center */}
          <p className="text-xl text-white mb-4">Like plants?</p>
          {/* Updated the anchor tag to NavLink */}
          <NavLink
            to="/signup"
            className="bg-white text-1acd81 py-2 px-6 rounded hover:bg-gray-100 transition duration-300 ease-in-out"
          >
            Join Us Today!
          </NavLink>
        </div>
      </div>
    </>
  );
}

export default MainPage;
