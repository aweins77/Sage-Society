import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";

const LogoutButton = () => {
  const { logout } = useToken();
  const navigate = useNavigate();

  const handleLogout = async () => {
    try {
      await logout();
      alert("Successfully logged out!"); // Provide feedback. Replace with a better method in production.
      navigate("/");
    } catch (error) {
      console.error("Error during logout:", error);
      alert("Error during logout. Please try again."); // Provide feedback. Replace with a better method in production.
    }
  };

  return (
    <button
      onClick={handleLogout}
      aria-label="Logout"
      className="bg-gradient-to-r from-red-400 to-orange-500 hover:from-orange-500 hover:to-red-400 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
    >
      Logout
    </button>
  );
};

export default LogoutButton;
