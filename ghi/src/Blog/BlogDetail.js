import React from "react";
import { useParams, NavLink } from "react-router-dom"; // Import NavLink here
import { useState, useEffect } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import backgroundImage from "./pexels-deeana-arts-2565222.jpg"; // 1. Import the image
import BlogPostList from "../BlogPost/BlogPostList";

export default function BlogDetail() {
  const { blogs_id } = useParams();
  const { token, fetchWithToken } = useToken();
  const [blog, setBlog] = useState();

  const fetchBlogDetail = async () => {
    const url = `${process.env.REACT_APP_API_HOST}/blogs/${blogs_id}`;
    const response = await fetchWithToken(url, "GET");
    if (response?.blogs_id != null)
    {
      setBlog(response);
    }
  };

  useEffect(() => {
    fetchBlogDetail();
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [blogs_id, token]);

  useEffect(() => {

  }, [blog]);

  return (
    <>
      {blog ? (
        <div
          style={{ backgroundImage: `url(${backgroundImage})` }} // 2. Apply the background image
          className="min-h-screen flex justify-center bg-cover bg-center bg-no-repeat"
        >
          <div className="mt-32 w-1/2">
            <div className="flex items-center justify-center">
              <div className="bg-white p-8 rounded-lg shadow-md w-full max-w-2xl mb-8">
                <h1 className="text-2xl mb-6 text-center font-bold">
                  {blog.title}
                </h1>
                {/* Directly use the NavLink here instead of wrapping it inside a button */}
                <NavLink
                  to={`/blog/${blogs_id}/blogpost`}
                  className="w-full block text-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                  activeClassName="text-black"
                >
                  Create a Post
                </NavLink>
              </div>
            </div>
            <BlogPostList/>
          </div>
        </div>
      ) : null}
    </>
  );
}
