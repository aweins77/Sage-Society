import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import CreateBlogForm from "./CreateBlogForm";

export default function Blog() {
  const navigate = useNavigate();
  const { token, fetchWithToken } = useToken();
  const [blog, setBlog] = useState(null);

  const fetchBlog = async () => {
    const blog = await fetchWithToken(
      `${process.env.REACT_APP_API_HOST}/blogs/account`,
      "GET"
    );
    setBlog(blog);
  };

  useEffect(() => {
    if (token) {
      fetchBlog();
    }
  }, [token]);

  useEffect(() => {
    if (blog !== null && blog?.blogs_id)
    {
      navigate(`/blog/${blog.blogs_id}`)
    }
  }, [blog]);

  return (
    <>
    <CreateBlogForm/>
    </>
  );
}
