import React from "react";
import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import backgroundImage from "../Blog/pexels-deeana-arts-2565222.jpg";


export default function PostDetail(){
    const{blogs_id,blogs_post_id}=useParams()
    const[blogpost,setBlogPost]=useState("");


    const fetchPost= async() =>{
        const url =`${process.env.REACT_APP_API_HOST}/blog/${blogs_id}/blogpost/${blogs_post_id}`;
        const response = await fetch (url,
            {credentials:"include",
        });
        if(response.ok){
            const data= await response.json();
            setBlogPost(data);
        }
    };
    useEffect(()=>{
    fetchPost();
    //eslint-disable-next-line react-hooks/exhaustive-deps
},[blogs_id,blogs_post_id]);

return(
<>
    {blogpost? (
        <>
        <div
          style={{ backgroundImage: `url(${backgroundImage})` }} // 2. Apply the background image
          className="min-h-screen flex justify-center bg-cover bg-center bg-no-repeat"
        >
            <div className="mt-32">
                <div className="bg-gray-100 p-8 rounded-lg shadow-md w-full max-w-2xl mb-8">
                    <h1 className="text-2xl mb-6 text-center mt-4 font-bold">{blogpost.title} </h1>
                    <div>
                        <img alt="blogpost" className="mb-2 h-80 w-80" src={blogpost.picture_url}/>
                        <p className="mb-2">{blogpost.text}</p>
                    </div>
                </div>
            </div>
        </div>
        </>
      ) : null}




</>
);

}
