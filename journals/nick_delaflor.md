# Nick de la Flor | Sage Society Development Journal

### Week 16 Entry:

- 09/04/2023 - 09/08/2023
- Implemented unit tests for get all plants and get one instance of a plant
- Created 3rd party API calls to both Trefle API and openAI GPT.
- Implemented styling consistent with rest of website.
- Worked on edit plant function for about two days, ended up just forcing input validation on user input.
- Created environment variables for 3rd party APIs.
- Tested all endpoints backend and frontend no errors.

### Week 15 Entry:

- 08 /28/2023 - 09/01/2023
- Updated components to acurrately reflect what I wanted displayed.
- Troubleshot CORS errors.
- Implemented upload an image file for plantspage on frontend.
- Worked through numerous backend issues.

### Week 14 Entry:

- 08 /21/2023 - 08/25/2023
- Tried to implement Tailwind css on GHI app.
- Created List all Plants page that would list all plants for the user
- Created plants page that listed plants based on current garden.
- Deleted list all plants page.
- Edited Nav bar and App.js for my components

---

### Week 13 Entry:

- 08/14/2023 - 08/18/2023
- This week we tried to implement our backend database design.
- I re-watched the FastAPI lecture and was able to implement most of backend endpoints without issues.
- In our group we went back and forth between how plants should build on top of gardens. I'm debating between implementing both a plants page that lists all plants as well as listing plants based on the user defined garden.
- Troubleshot backend.

### Week 12 Entry:

- 08/07/2023 - 08/11/2023
- Worked with group to come up with different ideas for a project
- Decided on gardening application
- I volunteered to implement a plants feature
- We created our wire diagrams on Trello and website flow
- We created our database schema. This allows us to see what pieces of data we will need to use and what piece of data is dependant on one another, as well as each of their relationships.
- In trello I wrote my API diagram. I am building this on top of the gardens id. Instructor Zach commented on this and asked if I was sure this was the approach I wanted to take. I wasn't sure tbh but since that's what they showed us in RESTful API's for mod 2 I decided to stick with this approach.
- We debated heavily about how best to approach the plants. Should it be user defined or should it be from a master database of plants. Based on instruction suggestion, we went with user defined plants.
- We discussed the possibilities of using Trefle API which is a plant database tool and how we might use it.

---

---
